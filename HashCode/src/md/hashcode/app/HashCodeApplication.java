package md.hashcode.app;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import md.hashcode.obj.InputObject;
import md.hashcode.obj.OuputObject;
import md.hashcode.services.DataParseService;
import md.hashcode.services.DataParser;
import md.hashcode.services.FileRead;
import md.hashcode.services.InputFileReader;
import md.hashcode.services.MainAlgorithm;
import md.hashcode.services.OutputFileWriter;

public class HashCodeApplication {

	public static String myFiles[] = { "a_example.txt", "b_read_on.txt", "c_incunabula.txt", "d_tough_choices.txt",
			"e_so_many_books.txt", "f_libraries_of_the_world.txt" };

	@SuppressWarnings("static-access")
	public static void main(String[] args) {

		InputFileReader fileReader = new FileRead();
		List<String> lines = new ArrayList<>();
		List<InputObject> listOfInputObject = new ArrayList<>();
		for (int i = 0; i < myFiles.length; i++) {
			lines = fileReader.readFromInputFile(myFiles[i]);
			
			DataParser parser = new DataParseService();
			listOfInputObject = parser.parse(lines, 2);
			
			//lines.stream().forEach(System.out::println);
			//listOfInputObject.forEach(l -> l.printLibrary());
			
			listOfInputObject.sort(Comparator.comparing(InputObject::getShip).thenComparing(InputObject::getSingUp).reversed());
			List<OuputObject> outputs = MainAlgorithm.doIt(listOfInputObject);
			
			List<String> outDatas = new ArrayList<>();
			String usedLibray = outputs.get(0).usedLibraryCount.toString();
			outDatas.add(usedLibray);
			for(OuputObject out : outputs) {
				String line1 = out.getLibraryId() + " " + out.getUsedBookCount();
				outDatas.add(line1);
				String line2 = out.getBooks().stream().map(b -> b.getBookId()).collect(Collectors.joining(" "));
				outDatas.add(line2);
			}
			
			OutputFileWriter fileWriter = new md.hashcode.services.FileWriter();
			try {
				fileWriter.writeToOutputFile("subFile_"+ new Integer(i).toString() +".txt", outDatas);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
