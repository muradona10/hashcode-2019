package md.hashcode.obj;

import java.util.List;
import java.util.stream.Collectors;

public class OuputObject {
	
	public static Integer usedLibraryCount;
	
	private String libraryId;
	
	private Integer usedBookCount;
	
	private List<Book> books;

	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	public Integer getUsedBookCount() {
		return usedBookCount;
	}

	public void setUsedBookCount(Integer usedBookCount) {
		this.usedBookCount = usedBookCount;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	public void printOutputObject() {
		System.out.println("-------------------------------------------------");
		System.out.println(usedBookCount);
		System.out.println(libraryId + " " + usedBookCount);
		System.out.println(books.stream().map(bx -> bx.getBookId()).collect(Collectors.joining(", ", "[", "]")));
	}
	
}
