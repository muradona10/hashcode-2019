package md.hashcode.obj;

import java.util.List;
import java.util.stream.Collectors;

public class Library {
	
	public static Integer allBookCount;
	public static Integer libraryCount;
	public static Integer dayCount;
	public static Integer remaingCount;
	
	private String libraryId;
	
	private Integer bookCount;
	
	private Integer singUp;
	
	private Integer ship;
	
	private List<Book> books;

	public Library(String libraryId, Integer bookCount, Integer singUp, Integer ship, List<Book> books) {
		this.libraryId = libraryId;
		this.bookCount = bookCount;
		this.singUp = singUp;
		this.ship = ship;
		this.books = books;
	}

	public Library() {
		super();
	}
	
	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	public Integer getBookCount() {
		return bookCount;
	}

	public void setBookCount(Integer bookCount) {
		this.bookCount = bookCount;
	}

	public Integer getSingUp() {
		return singUp;
	}

	public void setSingUp(Integer singUp) {
		this.singUp = singUp;
	}

	public Integer getShip() {
		return ship;
	}

	public void setShip(Integer ship) {
		this.ship = ship;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	@Override
	public String toString() {
		return "Library [bookCount=" + bookCount + ", singUp=" + singUp + ", ship=" + ship + ", books=" + books + "]";
	}
	
	public void printLibrary() {
		System.out.println("-------------------------------------------------");
		System.out.println(allBookCount + " " + libraryCount + " " + dayCount);
		System.out.println(libraryId + " " + bookCount + " " + singUp + " " + ship);
		System.out.println(books.stream().map(bx -> bx.getBookId()).collect(Collectors.joining(", ", "[", "]")));
		System.out.println(books.stream().map(bx -> bx.getBookScore().toString()).collect(Collectors.joining(", ", "[", "]")));
	}

	
}
