package md.hashcode.obj;

public class Book {
	
	private String bookId;
	
	private Integer bookScore;

	public Book(String bookId, Integer bookScore) {
		this.bookId = bookId;
		this.bookScore = bookScore;
	}

	public Book() {
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public Integer getBookScore() {
		return bookScore;
	}

	public void setBookScore(Integer bookScore) {
		this.bookScore = bookScore;
	}
	
	

}
