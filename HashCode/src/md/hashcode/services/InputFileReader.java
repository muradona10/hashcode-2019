package md.hashcode.services;

import java.util.List;

public interface InputFileReader {
	
	List<String> readFromInputFile(String fileName);

}
