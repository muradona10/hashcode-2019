package md.hashcode.services;

import java.io.IOException;
import java.util.List;

public interface OutputFileWriter {
	
	void writeToOutputFile(String fileName, List<String> dataList)  throws IOException;

}
