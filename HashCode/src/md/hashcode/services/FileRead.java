package md.hashcode.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileRead implements InputFileReader {

	@Override
	public List<String> readFromInputFile(String fileName) {
		
		try(Stream<String> lines = Files.lines(Paths.get(fileName))){
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}

}
