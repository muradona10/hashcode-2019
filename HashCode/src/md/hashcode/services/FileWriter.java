package md.hashcode.services;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class FileWriter implements OutputFileWriter {

	@Override
	public void writeToOutputFile(String fileName, List<String> dataList) throws IOException {
		
		FileOutputStream fos = new FileOutputStream(fileName);
		
		dataList.stream().forEach(line -> {
			
			try {
				fos.write(line.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		});
		
		fos.flush();
		fos.close();

	}

}
