package md.hashcode.services;

import java.util.List;

import md.hashcode.obj.InputObject;

public interface DataParser {
	
	List<InputObject> parse(List<String> dataList, int dataLineCount);

}
