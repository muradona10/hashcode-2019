package md.hashcode.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import md.hashcode.obj.Book;
import md.hashcode.obj.InputObject;

public class DataParseService implements DataParser {

	@SuppressWarnings("static-access")
	@Override
	public List<InputObject> parse(List<String> dataList, int dataLineCount) {

		InputObject in = new InputObject();
		in.allBookCount = Integer.parseInt(dataList.get(0).split(" ")[0]);
		in.libraryCount = Integer.parseInt(dataList.get(0).split(" ")[1]);
		in.dayCount = Integer.parseInt(dataList.get(0).split(" ")[2]);
		in.remaingCount = in.dayCount;

		Map<String, Book> bookMap = new HashMap<>();
		String[] bookArray = dataList.get(1).split(" ");
		for (int i = 0; i < bookArray.length; i++) {
			String bookId = new Integer(i).toString();
			Integer bookScore = Integer.parseInt(bookArray[i]);
			Book b = new Book(bookId, bookScore);
			bookMap.put(bookId, b);
		}
				
        List<String> myDataList = dataList.stream().skip(2).collect(Collectors.toList());
		List<InputObject> list = new ArrayList<>();
		int library = 0;
		for (int i = 0; i < myDataList.size(); i = i + dataLineCount) {
			
			InputObject inputObject = new InputObject();
			inputObject.setLibraryId(new Integer(library).toString());
			library++;
			
			// Need necessary code to create data objects - begin
			Integer bookCount = Integer.parseInt(myDataList.get(i).split(" ")[0]);
			Integer singUp = Integer.parseInt(myDataList.get(i).split(" ")[1]);
			Integer ship = Integer.parseInt(myDataList.get(i).split(" ")[2]);
			
			List<Book> books = new ArrayList<>();
			String[] lBookArray = myDataList.get(i+1).split(" ");
			for (int j = 0; j < lBookArray.length; j++) {
				String bookId = lBookArray[j];
				Integer bookScore = bookMap.get(bookId).getBookScore();
				Book b = new Book(bookId, bookScore);
				books.add(b);
			}
			
			inputObject.setBookCount(bookCount);
			inputObject.setSingUp(singUp);
			inputObject.setShip(ship);
			inputObject.setBooks(books);

			// Need necessary code to create data objects - end
			
			
			//inputObject.printLibrary();
			list.add(inputObject);
		}

		return list;
	}

}
