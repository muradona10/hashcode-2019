package md.hashcode.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import md.hashcode.obj.Book;
import md.hashcode.obj.InputObject;
import md.hashcode.obj.OuputObject;

public class MainAlgorithm {
	
	@SuppressWarnings("static-access")
	public static List<OuputObject> doIt(List<InputObject> inputs){
		
		List<OuputObject> outs = new ArrayList<>();
		for(InputObject in : inputs) {
			
			OuputObject out = new OuputObject();
			
			in.remaingCount = in.dayCount - in.getSingUp();
			in.dayCount = in.dayCount - in.getSingUp();
			
			if (in.remaingCount > 0) {
				out.setLibraryId(in.getLibraryId());
				long availableBookCount = (long) in.remaingCount * in.getShip();
				if (availableBookCount > in.getBookCount()) {
					out.setUsedBookCount(in.getBookCount());
					out.setBooks(in.getBooks());
				} else {
					out.setUsedBookCount((int) availableBookCount);
					List<Book> usedBooks = in.getBooks();
					usedBooks.sort(Comparator.comparing(Book::getBookScore));
					out.setBooks(usedBooks.subList(0, in.remaingCount * in.getShip()));
				}
				
				//out.printOutputObject();
				outs.add(out);
				out.usedLibraryCount = outs.size();
			}
			
		}
		
		return outs;
	}

}
